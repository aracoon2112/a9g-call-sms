#include "api_hal_gpio.h"
#include <string.h>
#include <stdio.h>
#include <api_os.h>
#include <api_event.h>
#include <api_hal_uart.h>
#include <api_debug.h>
#include "buffer.h"
#include "api_hal_pm.h"
#include "time.h"
#include "api_info.h"

#include "stdbool.h"
#include "stdint.h"

#include "api_call.h"
#include "api_audio.h"
#include "api_sms.h"

#ifndef DIAL_NUMBER
#define DIAL_NUMBER "0972806346"
#endif // !
#define SEND_SMS 1
#define MAIN_TASK_STACK_SIZE    (2048 * 2)
#define MAIN_TASK_PRIORITY      0
#define MAIN_TASK_NAME          "Pyroject test task"

static HANDLE mainTaskHandle = NULL;
static HANDLE startCall=NULL;
void OnPinFalling();
void CallTest();
uint8_t flag = 0;
uint8_t level= 0;
uint8_t level2=0;
bool isDialSuccess = false;
static int calling =0;
const uint8_t utf8Msg[]    = "Lộc đang test";
GPIO_config_t gpioINT = {
        .mode               = GPIO_MODE_INPUT,
        .pin                = GPIO_PIN30,
        .defaultLevel       = GPIO_LEVEL_HIGH,
        .intConfig.debounce = 0,
        .intConfig.type     = GPIO_INT_TYPE_MAX,
        .intConfig.callback = NULL
};


GPIO_config_t led = {
     .mode               = GPIO_MODE_OUTPUT,
        .pin                = GPIO_PIN29,
        .defaultLevel       = GPIO_LEVEL_LOW,
        .intConfig.debounce = 0,
        .intConfig.type     = GPIO_INT_TYPE_MAX,
        .intConfig.callback = NULL
};
GPIO_config_t led2 = {
     .mode               = GPIO_MODE_OUTPUT,
        .pin                = GPIO_PIN25,
        .defaultLevel       = GPIO_LEVEL_LOW,
        .intConfig.debounce = 0,
        .intConfig.type     = GPIO_INT_TYPE_MAX,
        .intConfig.callback = NULL
};
void SMSInit()
{
    if(!SMS_SetFormat(SMS_FORMAT_TEXT,SIM0))
    {
        Trace(1,"sms set format error");
        return;
    }
    SMS_Parameter_t smsParam = {
        .fo = 17 ,
        .vp = 167,
        .pid= 0  ,
        .dcs= 8  ,//0:English 7bit, 4:English 8 bit, 8:Unicode 2 Bytes
    };
    if(!SMS_SetParameter(&smsParam,SIM0))
    {
        Trace(1,"sms set parameter error");
        return;
    }
    if(!SMS_SetNewMessageStorage(SMS_STORAGE_SIM_CARD))
    {
        Trace(1,"sms set message storage fail");
        return;
    }
}

void blink_led(GPIO_config_t,uint8_t*);
void UartInit()
{
    UART_Config_t config = {
        .baudRate = UART_BAUD_RATE_115200,
        .dataBits = UART_DATA_BITS_8,
        .stopBits = UART_STOP_BITS_1,
        .parity   = UART_PARITY_NONE,
        .rxCallback = NULL,
    };
    UART_Init(UART1,config);
}

void Init()
{
    UartInit();
    SMSInit();
}

void SendUtf8(const char* number)
{
    uint8_t* unicode = NULL;
    uint32_t unicodeLen;

    Trace(1,"sms start send UTF-8 message");

    if(!SMS_LocalLanguage2Unicode(utf8Msg,strlen(utf8Msg),CHARSET_UTF_8,&unicode,&unicodeLen))
    {
        Trace(1,"local to unicode fail!");
        return;
    }
    while (!SMS_SendMessage(number,unicode,unicodeLen,SIM0))
    {
        blink_led(led2,&level2);
        OS_Sleep(500);
    }
    OS_Free(unicode);
}

void EventDispatch(API_Event_t* pEvent)
{
    static uint8_t lbsCount = 0;
    switch(pEvent->id)
    {
        case API_EVENT_ID_NO_SIMCARD:
            Trace(10,"!!NO SIM CARD%d!!!!",pEvent->param1);
            break;

        case API_EVENT_ID_SYSTEM_READY:
            Trace(1,"system initialize complete");
            break;

        case API_EVENT_ID_NETWORK_REGISTERED_HOME:
        case API_EVENT_ID_NETWORK_REGISTERED_ROAMING:
            Trace(1,"network register success");
            flag = 1;
            break;
        case API_EVENT_ID_CALL_DIAL://param1: isSuccess, param2:error code(CALL_Error_t)
            Trace(1,"Is dial success:%d, error code:%d",pEvent->param1,pEvent->param2);
            if(pEvent->param1)
                isDialSuccess = true;
            break;
        case API_EVENT_ID_CALL_HANGUP:  //param1: is remote release call, param2:error code(CALL_Error_t)
            isDialSuccess=1;
            Trace(1,"Hang up,is remote hang up:%d, error code:%d",pEvent->param1,pEvent->param2);
            break;
        case API_EVENT_ID_CALL_INCOMING:   //param1: number type, pParam1:number
            Trace(1,"Receive a call, number:%s, number type:%d",pEvent->pParam1,pEvent->param1);
            OS_Sleep(5000);
            if(!CALL_Answer())
                Trace(1,"answer fail");
            break;
        case API_EVENT_ID_CALL_ANSWER  :  
            Trace(1,"answer success");
            break;
        case API_EVENT_ID_CALL_DTMF    :  //param1: key
            Trace(1,"received DTMF tone:%c",pEvent->param1);
            break;
        case API_EVENT_ID_SMS_SENT:
            Trace(2,"Send Message Success");
           
            break;
        default:
            break;
    } 
}


void CallTest()
{
    //initialize
    Trace(1,"start Call test, initialize audio first");
    AUDIO_MicOpen();
    AUDIO_SpeakerOpen();
    AUDIO_MicSetMute(false);
    AUDIO_SpeakerSetVolume(15);
    isDialSuccess=0;
    //make a call
    Trace(1,"start make a call %s",DIAL_NUMBER);
    if(!CALL_Dial(DIAL_NUMBER))
    {
        Trace(1,"make a call failed");
        return;
    }
    while(!isDialSuccess){
        blink_led(led,&level);
       OS_Sleep(100);
    }
    GPIO_SetLevel(led,GPIO_LEVEL_LOW);
    Trace(1,"complete");
    
   
}



void project_MainTask(){
    Init();
    GPIO_Init(led);
    GPIO_Init(led2);
 GPIO_Init(gpioINT);
 static GPIO_LEVEL status=1;
 // GPIO_GetLevel(gpioINT,&status);
 int start =0 ;
 while(1){
     GPIO_LEVEL preStatus = status;
      GPIO_GetLevel(gpioINT,&status);
      OS_Sleep(500);
        if ((clock()-start)/CLOCKS_PER_SEC>=5){ blink_led(led2,&level2);
         start=clock();}
     if (!status&& preStatus!=status)  {
    SendUtf8(DIAL_NUMBER);
         #if SEND_SMS
    SendUtf8("0914662677");
    SendUtf8("0914267775");
    SendUtf8("0975292305");
    SendUtf8("0392661489");
    SendUtf8("0795778418");
         #endif
             CallTest();

             }
     OS_Sleep(50);
 }   
}



 void callTask(){
     if (calling)
     {
         CallTest();
     }
     calling=0;
 }

void MainTask(void* param){
    API_Event_t* event ;
   

    OS_CreateTask(project_MainTask,
        NULL ,NULL, MAIN_TASK_STACK_SIZE,MAIN_TASK_PRIORITY, 0,0,MAIN_TASK_NAME );

    while (1){
        if(OS_WaitEvent(mainTaskHandle,(void**)&event,OS_TIME_OUT_WAIT_FOREVER)){
            EventDispatch(event);
            OS_Free(event->pParam1);
            OS_Free(event->pParam2);
            OS_Free(event);
        }
    }
}
void project_Main(void){
    mainTaskHandle = OS_CreateTask(MainTask,
        NULL,NULL,MAIN_TASK_STACK_SIZE,MAIN_TASK_PRIORITY,0,0,MAIN_TASK_NAME);
        OS_SetUserMainHandle(&mainTaskHandle);
}
void blink_led(GPIO_config_t some_led ,uint8_t* some_level){
    *some_level = !*some_level;
    GPIO_SetLevel(some_led,*some_level);
}